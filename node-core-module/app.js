//belajar module readline

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('masukkan namamu : ', (nama) => {
    rl.question('masukkan umur :', (umur) => {
        // siapkan data 
        const dataKu = {
            nama: nama,
            umur: umur
        }
        //baca filenya dulu dan tampung
        const dataBuffer = fs.readFileSync('contacts.json', 'utf-8')
        const dataJSON = JSON.parse(dataBuffer);


        dataJSON.push(dataKu);
        console.log(dataJSON);

        dataKirim = JSON.stringify(dataJSON, null, 2);

        //tulis ke file
        try {
            fs.writeFileSync('contacts.json', dataKirim);

        } catch (e) {
            console.log(e)
        }

        rl.close();
    });

});

const fs = require('fs');

// membuat file dengan syncronus


// membuat file dengan cara asyncronus
// fs.writeFile("data.txt", "ini asyncronus file", (e) => {
//     console.log(e);
// })

// membaca file dengan syncronus
// const data = fs.readFileSync("data.txt", "utf-8");
// console.log(data);

//membaca file dengan asyncronus

// fs.readFile('data.txt', 'utf-8', (err, data) => {
//     if (err) throw err;
//     console.log(data)
// })


